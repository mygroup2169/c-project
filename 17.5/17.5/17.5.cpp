﻿#include <iostream>
#include <cmath>

class Vector {
public:
    Vector(float newA, float newB, float newC) {
        a = newA;
        b = newB;
        c = newC;
    }
    void setNewVector(float newA, float newB, float newC) {
        a = newA;
        b = newB;
        c = newC;
    }
    float getModulus() {
        float result = sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2));
        return result;
    }
private:
    float a;
    float b;
    float c;
};

int main()
{
    Vector temp (5, 4, 6);
    std::cout << "Vector modulus: " << temp.getModulus() << "\n";
    float a = 5;
    float b = 9;
    float c = 10;
    temp.setNewVector(a, b, c);
    std::cout << "Vector modulus: " << temp.getModulus() << "\n";
}