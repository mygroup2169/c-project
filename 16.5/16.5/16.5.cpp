﻿#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    const int n = 5;
    int array[n][n]{};
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }
    int numberFromDay = buf.tm_mday % n;
    int finalSum = 0;
    for (int i = 0; i < n; i++) {
        finalSum = finalSum + array[numberFromDay][i];
    }
    std::cout << "Sum of " << numberFromDay << " array row: " << finalSum;
}