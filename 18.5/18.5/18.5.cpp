﻿#include <iostream>

class Player {
public:
    Player() {
        name = "";
        points = 0;
    }

    Player(int newPoints, std::string newName) {
        name = newName;
        points = newPoints;
    }

    void showName() {
        std::cout << "Name: " << name << " Points: " << points;
    }
    int getPoints() {
        return points;
    }

private:
    std::string name;
    int points;
};

void addPlayers(int numberOfPlayers, Player* players) {
    int newPoints = 0;
    std::string newName;
    for (int i = 0; i < numberOfPlayers; i++) {
        std::cout << "Enter the name: ";
        std::cin >> newName;
        std::cout << "Enter the points: ";
        std::cin >> newPoints;
        Player newPlayer(newPoints, newName);
        players[i] = newPlayer;
    }
}

void showPlayers(int numberOfPlayers, Player* players) {
    for (int i = 0; i < numberOfPlayers; i++) {
        players[i].showName();
        std::cout << "\n";
    }
}

void sort(int numberOfPlayers, Player* players) {
    for (int i = 1; i < numberOfPlayers; i++) {
        for (int j = i; j > 0 && players[j - 1].getPoints() < players[j].getPoints(); j--) {
            Player Tmp = players[j - 1];
            players[j - 1] = players[j];
            players[j] = Tmp;
        }
    }
}

int main()
{
    std::cout << "Enter the number of players: \n";
    int numberOfPlayers = 0;
    std::cin >> numberOfPlayers;
    Player* players = new Player [numberOfPlayers];
    addPlayers(numberOfPlayers, players);
    sort(numberOfPlayers, players);
    showPlayers(numberOfPlayers, players);
    delete[] players;
}