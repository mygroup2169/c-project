﻿#include <iostream>

class Animal {
public:
    Animal() {};
    virtual void voice() {
        std::cout << "Some voice\n";
    };
};

class Dog : public Animal {
public:
    void voice() override {
        std::cout << "Bark!\n";
    }
};

class Cat : public Animal {
public:
    void voice() override {
        std::cout << "Meow!\n";
    }
};

class Pig : public Animal {
public:
    void voice() override {
        std::cout << "Oink!\n";
    }
};

class Frog : public Animal {
public:
    void voice() override {
        std::cout << "Croak!\n";
    }
};

class Cow : public Animal {
public:
    void voice() override {
        std::cout << "Moo!\n";
    }
};


int main()
{
    int arraySize = 5;
    Cat cat;
    Dog dog;
    Pig pig;
    Frog frog;
    Cow cow;
    Animal** animals = new Animal* [5] {&cat, & frog, & dog, & pig, & cow};
    for (int i = 0; i < 5; i++) {
        animals[i]->voice();
    }
    delete[] animals;
}