﻿#include <iostream>

void FindEvenNumbers(int Limit) {
    if (Limit < 0) {
        std::cout << "Uncorrect number!";
    } else {
        std::cout << "Even numbers:\n";
        for (int i = 0; i <= Limit; i = i + 2) {
            std::cout << i << "\n";
        }
    }
}

void FindOddNumbers(int Limit) {
    if (Limit < 0) {
        std::cout << "Uncorrect number!";
    }
    else {
        std::cout << "Odd numbers:\n";
        for (int i = 1; i <= Limit; i = i + 2) {
             std::cout << i << "\n";
        }
    }
}


int main()
{
    std::cout << "This application will display odd and even numbers from 1 to n.\n";
    std::cout << "Enter the n: ";
        int n = 0;
        std::cin >> n;
        FindEvenNumbers(n);
        FindOddNumbers(n);
}