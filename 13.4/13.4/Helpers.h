#pragma once
#include <cmath>

double evenDigits(double number1, double number2) {
    double result = number1 + number2;
    return pow(result, 2);
}