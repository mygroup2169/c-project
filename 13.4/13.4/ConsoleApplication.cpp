﻿#include <iostream>
#include "Helpers.h"

int main()
{
    std::cout << "This program will sums two numbers and return their square.\n";
    std::cout << "Enter the first number:\n";
    double number1;
    std::cin >> number1;
    std::cout << "Enter the second number:\n";
    double number2;
    std::cin >> number2;
    std::cout << "Result:\n";
    std::cout << evenDigits(number1, number2);
}
