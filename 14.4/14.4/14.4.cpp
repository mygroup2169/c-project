﻿#include <iostream>

int main()
{
    std::cout << "Enter the string:\n";
    std::string str;
    std::cin >> str;
    std::cout << "String length:\n";
    std::cout << str.length() << "\n";
    std::cout << "First char:\n";
    std::cout << str[0] << "\n";
    std::cout << "Last char:\n";
    std::cout << str[str.length()-1] << "\n";
}